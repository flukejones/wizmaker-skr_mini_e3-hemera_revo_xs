#include <Average.h>   // Source - https://github.com/MajenkoLibraries/Average
#define DEBUG 1

const int ledPin = 13;      // LED is connected to digital pin 13
const int relayPin = 11;    // Optocoupler output is connected to digital pin D3
const int piezoPin = A0;

// Sudden stops and starts can trigger this, so marlin z-probe may need adjusting
const int threshold = 2;    // threshold: Lower is more sensitive. Ended up approx 0.2mm in to bed

Average<int> average(128);
int sensorReading = 0;
int meanValue = 0;
int highest = 0;

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(relayPin, OUTPUT);
  digitalWrite(ledPin, LOW);
  digitalWrite(relayPin, LOW);
  if (DEBUG == 1){
    Serial.begin(250000);
  }
}

void loop() {
  sensorReading = analogRead(piezoPin);
  average.push(sensorReading);
  meanValue = average.mean();
  
  if (DEBUG == 1){
    Serial.print(sensorReading);
    Serial.print(",");
    Serial.print(meanValue); // Good reference during an M48 repeatability test
    Serial.print(",");
    Serial.print(highest);
    Serial.print(",");
    Serial.println(threshold);
    Serial.print(" ");
  }
  if (meanValue > threshold) {
    if (meanValue > highest) {
      highest = meanValue;
    }
    digitalWrite(ledPin, HIGH);
    digitalWrite(relayPin, LOW);
    // once we've triggered output, we stay on for 0.05 sec to make printer really recognize it.
    delayMicroseconds(50000);
  } else {
    digitalWrite(ledPin, LOW);
    digitalWrite(relayPin, HIGH);
  }
}
