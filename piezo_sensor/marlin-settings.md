# Piezo stuff

In general, you want to enable the piezo to be as sensitive as possible, and then minimise noise plus set any offsets etc afterwards.

#### Endstop logic configuration

Comment #define ENDSTOPPULLUP_ZMIN (or other pin if using a different connection)

Set Z_MIN_ENDSTOP_INVERTING to “true” maybe?

#### Enable Interrupts

Uncomment this line for best possible accuracy if supported:

`#define ENDSTOP_INTERRUPTS_FEATURE`

#### Configure Z-Probe Options

- uncomment:

  `#define FIX_MOUNTED_PROBE`

- reduce noise interference and false triggers

  `#define PROBING_FANS_OFF // remove fan noise`

  `#define DELAY_BEFORE_PROBING 250 // the up/down jerk can cause a false trigger`

- Set all probe offsets to 0

`#define NOZZLE_TO_PROBE_OFFSET { 0, 0, 0.0 }`

- Change Z_PROBE_SPEED_SLOW to match FAST, the piezo works best with suddenness not "slowly pressing":

  `#define Z_PROBE_FEEDRATE_SLOW (Z_PROBE_FEEDRATE_FAST)`

- Optional, Uncomment:

  `#define MULTIPLE_PROBING 2`

- Uncomment:

  `#define Z_MIN_PROBE_REPEATABILITY_TEST`

#### Configure Bed Levelling

- Use either `AUTO_BED_LEVELING_BILINEAR` for simpler levelling, or `AUTO_BED_LEVELING_UBL` for a full suite of goodies.

- Optional: Uncomment `#define RESTORE_LEVELING_AFTER_G28` if you don't plan on doing a full G29 after each G28

- Uncomment and set `#define MANUAL_Z_HOME_POS -0.15`

- Uncomment `#define Z_SAFE_HOMING`
  - Default for this setting puts the nozzle in the center of the bed for G28Z, but you can define a corner offset if you wish. This will be performed before any bed levelling procedure

#### Additional tweaks

- Set homing to be faster

  `#define HOMING_FEEDRATE_MM_M { (120*60), (120*60), (8*60) }`

- Tweak the speeds of probing

  ```
  #define XY_PROBE_FEEDRATE (150*60)
  #define Z_PROBE_FEEDRATE_FAST (8*60)
  #define Z_PROBE_FEEDRATE_SLOW (Z_PROBE_FEEDRATE_FAST / 2) // Should be a minimum of 4*60
  ```

  Alternatively `Z_PROBE_FEEDRATE_FAST` and `Z_PROBE_FEEDRATE_FAST` can be set to the same for single probe of each mesh points

- The `DEFAULT_MAX_FEEDRATE` may need adjusting to suit the above.

- Set `Z_CLEARANCE_DEPLOY_PROBE` to `3` and `Z_CLEARANCE_BETWEEN_PROBES` + `Z_CLEARANCE_MULTI_PROBE` to smaller (3).

- Set `Z_HOMING_HEIGHT` & `Z_AFTER_HOMING` to match the above clearance to avoid excessive moves. I set:

  ```
  #define Z_AFTER_HOMING Z_HOMING_HEIGHT
  #define Z_CLEARANCE_DEPLOY_PROBE   Z_HOMING_HEIGHT
  #define Z_CLEARANCE_BETWEEN_PROBES  Z_CLEARANCE_DEPLOY_PROBE
  #define Z_CLEARANCE_MULTI_PROBE     Z_CLEARANCE_DEPLOY_PROBE
  ```

- Set `GRID_MAX_POINTS_X` to suit preferred granularity. It's worth also setting `ABL_BILINEAR_SUBDIVISION` & `RESTORE_LEVELING_AFTER_G28`.

- Check `Z_PROBE_END_SCRIPT` for movements desired.

- Check `STARTUP_COMMANDS` for loading commands such as `#define STARTUP_COMMANDS "M501\nM150 U45 S0\n"`

## Z-Offset

Do a home of Z, then check the height result. The nozzle may need to be moved up in steps until a bit of 80gsm paper can slide under it with some friction. Once this is achieved set with `M851 Z<final height>`.
